require 'xcodeproj'
require 'fileutils'

require_relative './dependencies/bundler'
require_relative './dependencies/yarn'
require_relative './dependencies/npm'
require_relative './dependencies/cocoapods'
require_relative './dependencies/carthage'

begin
  puts "\e[34;1m--INPUTS--\e[0m"
  puts "platform: #{ENV['platform']}"
  puts "yarn_build_platform: #{ENV['yarn_build_platform']}"
  puts "yarn_install_flags: #{ENV['yarn_install_flags']}"
  puts "yarn_cache: #{ENV['yarn_cache']}"
  puts "npm_install_flags: #{ENV['npm_install_flags']}"
  puts "npm_cache: #{ENV['npm_cache']}"
  puts "pods_cache: #{ENV['pods_cache']}"
  puts "carthage_cache: #{ENV['carthage_cache']}"
  puts "\e[34;1m----------\e[0m"

  install_bundler_dependencies()
  install_yarn_dependencies()
  install_npm_dependencies()

  if ENV['platform'].length <= 0 || ENV['platform'] == 'ios'
    install_carthage_dependencies()
    install_cocoapod_dependencies()
  end

  # Add the cache
  `envman add --key "BITRISE_CACHE_INCLUDE_PATHS" --value "#{ENV["BITRISE_CACHE_INCLUDE_PATHS"]}"`

rescue => ex
  puts
  puts 'Error:'
  puts ex.to_s
  puts
  puts 'Stacktrace (for debugging):'
  puts ex.backtrace.join("\n").to_s
  exit 1
end
