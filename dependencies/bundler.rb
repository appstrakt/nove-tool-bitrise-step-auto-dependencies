require 'find'
require 'rubygems'
require 'bundler/setup'

def install_bundler_dependencies
  #Find all the gemfile.lock files
  bundler_directory_paths = Dir.glob("**/Gemfile.lock").reject{ |f| f['node_modules'] || f['Carthage'] || f['Pods'] || f['Demo'] }.map{ |s| File.dirname(s) }

  if bundler_directory_paths.length > 0
    puts "########## BUNDLER ############"
    bundler_directory_paths.each do | directory |
      Dir.chdir(directory) do
        Bundler.with_clean_env do
          puts "File: #{directory}/Gemfile.lock"
          puts "Executing `bundle install`"
          raise "bundle install failed" unless system("bundle install --gemfile Gemfile")
        end
      end
    end
    puts "######### END BUNDLER #########"
  end
end