require 'find'
require 'rubygems'
require 'bundler/setup'

def version_from_podfile
  return File.read("Podfile.lock")[/COCOAPODS: (.+)/,1]
end

def install_pods(cocoapods_version)
  install_command = cocoapods_version ? "pod _#{cocoapods_version}_ install --no-repo-update" : "pod install --no-repo-update"
  puts install_command
  if not system(install_command)
    puts "Pod install failed..."
    system("pod repo update")
    raise "Pod installation failed" unless system(install_command)
  end
end

def install_pods_bundler
  Bundler.with_clean_env do
    install_command = "bundle exec pod install --no-repo-update"
    puts install_command
    if not system(install_command)
      puts "Pod install failed..."
      system("bundle exec pod repo update")
      raise "Pod installation failed" unless system(install_command)
    end
  end
end

def install_cocoapods
  version = version_from_podfile()
  if version && version.length > 0
    if system("gem list cocoapods -i --version #{version} > /dev/null")
      install_pods(version)
    else
      puts "Version '#{version}' is currently not installed.\nInstalling...\n"
      system("gem install cocoapods -v #{version}")
      install_pods(version)
    end
  else
    "No pod version found... Using system version"
    system("gem install cocoapods") unless system("pod --version")
    install_pods()
  end
end

def install_cocoapod_dependencies
  #Find all the podfile.locks jsons
  pod_directory_paths = Dir.glob("**/Podfile.lock").reject{ |f| f['node_modules'] || f['Carthage'] || f['Pods'] || f['Demo'] }.map{ |s| File.dirname(s) }

  if pod_directory_paths.length > 0
    puts "########## COCOAPODS ############"

    pod_directory_paths.each do | directory |
      Dir.chdir(directory) do
        puts "File: #{directory}/Podfile.lock"

        if File.file?("Gemfile.lock")
          install_pods_bundler()
        else
          install_cocoapods()
        end

        cache_pods = ENV['pods_cache']
        if cache_pods == 'yes'
          # Cache the folders
          paths = ENV["BITRISE_CACHE_INCLUDE_PATHS"]
          paths = "" unless paths
          paths += "\n#{directory}/Pods -> #{directory}/Podfile.lock"
          ENV["BITRISE_CACHE_INCLUDE_PATHS"] = paths
        end
      end
    end
    puts "######### END COCOAPODS #########"
  end
end
