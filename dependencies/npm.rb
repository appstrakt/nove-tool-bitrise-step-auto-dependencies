require 'find'

def install_npm_dependencies
   #Find all the package-lock files
  npm_directory_paths = Dir.glob("**/package-lock.json").reject{ |f| f['node_modules'] || f['Carthage'] || f['Pods'] || f['Demo'] }.map{ |s| File.dirname(s) }

  platform = ENV['platform']

  if npm_directory_paths.length > 0
    puts "########## NPM ############"
    npm_directory_paths.each do | directory |
      Dir.chdir(directory) do
        puts "File: #{directory}/package-lock.json"
        puts "Executing `npm ci #{ENV['npm_install_flags']}`"
        raise "npm install failed" unless system("npm ci #{ENV['npm_install_flags']}")

        if platform.length > 0
          if File.readlines("./package.json").grep(/build:#{platform}/).size > 0
            puts "Executing `npm run build:#{platform}`"

            if system("npm run build:#{platform}") != 0
              warn "npm run build:#{platform} failed -> No `build:#{platform}` in package.json"

              puts "Executing `npm run build`"
              warn "npm run build failed" unless system("npm run build")
            end
          end
        elsif File.readlines("./package.json").grep(/build/).size > 0
          puts "Executing `npm run build`"
          warn "npm run build failed" unless system("npm run build")
        end

        cache_npm = ENV['npm_cache']
        if cache_npm == 'yes'
          # Cache the folders
          puts "Caching the node_modules"
          paths = ENV["BITRISE_CACHE_INCLUDE_PATHS"]
          paths = "" unless paths
          paths += "\n#{directory}/node_modules -> #{directory}/package-lock.json"
          ENV["BITRISE_CACHE_INCLUDE_PATHS"] = paths
        end
      end
    end
    puts "######### END NPM #########"
  end
end
