require 'find'

def install_yarn_dependencies
   #Find all the yarn.lock files
  yarn_directory_paths = Dir.glob("**/yarn.lock").reject{ |f| f['node_modules'] || f['Carthage'] || f['Pods'] || f['Demo'] }.map{ |s| File.dirname(s) }

  platform = ENV['platform']

  if yarn_directory_paths.length > 0
    puts "########## YARN ############"
    yarn_directory_paths.each do | directory |
      Dir.chdir(directory) do
        puts "File: #{directory}/yarn.lock"
        puts "Executing `yarn install #{ENV['yarn_install_flags']}`"
        raise "yarn install failed" unless system("yarn install #{ENV['yarn_install_flags']}")

        if platform.length > 0
          if File.readlines("./package.json").grep(/build:#{platform}/).size > 0
            puts "Executing `yarn build:#{platform}`"

            if system("yarn build:#{platform}") != 0
              warn "yarn build:#{platform} failed -> No `build:#{platform}` in package.json"

              puts "Executing `yarn build`"
              warn "yarn build failed" unless system("yarn build")
            end
          end
        elsif File.readlines("./package.json").grep(/build/).size > 0
          puts "Executing `yarn build`"
          warn "yarn build failed" unless system("yarn build")
        end

        cache_yarn = ENV['yarn_cache']
        if cache_yarn == 'yes'
          # Cache the folders
          puts "Caching the node_modules"
          paths = ENV["BITRISE_CACHE_INCLUDE_PATHS"]
          paths = "" unless paths
          paths += "\n#{directory}/node_modules -> #{directory}/yarn.lock"
          ENV["BITRISE_CACHE_INCLUDE_PATHS"] = paths
        end
      end
    end
    puts "######### END YARN #########"
  end
end
