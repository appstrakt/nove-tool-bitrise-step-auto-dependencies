require 'find'
require 'xcodeproj'

def get_platforms
  project_paths = Dir.glob("*.xcodeproj")

  if !project_paths || project_paths.length == 0
    puts "No .xcodeproj found"
    return
  end

  if project_paths.length > 1
    puts "Found multiple projects... Taking the first one."
  end

  project_path = project_paths[0]
  project = Xcodeproj::Project.open(project_path)

  if not project
    puts "'#{project_path}' is not a valid xcode project"
    return
  end

  platform_names = project.targets.map{ |target| target.platform_name }.uniq.join(",")

  project.save()

  return platform_names
end

def get_xcode_version
  version_string = `/usr/bin/xcodebuild -version | awk '/Xcode/ {print $2}'`
  return Gem::Version.new(version_string)
end


def install_carthage_dependencies
  #Find all the package jsons (But not in /node_modules)
  carthage_directory_paths = Dir.glob("**/{C,c}artfile").reject{ |f| f['node_modules'] || f['Carthage'] || f['Pods'] || f['Demo'] }.map{ |s| File.dirname(s) }

  if carthage_directory_paths.length > 0
    puts "########## CARTHAGE ############"

    carthage_directory_paths.each do | directory |
      Dir.chdir(directory) do
        puts "File: #{directory}/Cartfile"

        # Detect platforms
        platforms = get_platforms()

        carthage_command = "carthage bootstrap"

        if platforms && platforms.length > 0
          carthage_command += " --platform #{platforms}"
        end

        carthage_command += " --cache-builds"

        xcode_version = get_xcode_version()
        puts "Xcode version: #{xcode_version}"

        if xcode_version >= Gem::Version.new('13')
          carthage_command += " --use-xcframeworks"
        end

        puts carthage_command

        raise "carthage bootstrap failed" unless system(carthage_command)

        cache_carthage = ENV['carthage_cache']
        if cache_carthage == 'yes'
          # Cache the folders
          paths = ENV["BITRISE_CACHE_INCLUDE_PATHS"]
          paths = "" unless paths
          paths += "\n#{directory}/Carthage -> #{directory}/Cartfile.resolved"
          ENV["BITRISE_CACHE_INCLUDE_PATHS"] = paths
        end
      end
    end
    puts "######### END CARTHAGE #########"
  end
end
